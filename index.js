/**
 * mark > 90 = A+
 * mark > 80 = A
 * mark > 70 = B+
 * mark > 60 = B
 * mark > 50 = C+
 * mark > 40 = C
 * mark < 40 = failed
 */
let mark = 39
if (mark >= 90){
    console.log("A+")
}
else if (mark >= 80){
    console.log("A")
}
else if (mark >= 70){
    console.log("b+")
}
else if(mark >= 60){
    console.log("b")
}
else if (mark >= 50){
    console.log("c+")
}
else if (mark >= 40){
    console.log("c")
}
else {
    console.log("failed")
}
